# An exhibit of Markdown

This note demonstrates some of what [Markdown][1] is capable of doing.

*Note: Feel free to play with this page. Unlike regular notes, this doesn't automatically save itself.*

## Basic formatting

Paragraphs can be written like so. A paragraph is the basic block of Markdown. A paragraph is what text will turn into when there is no reason it should become anything else.

Paragraphs must be separated by a blank line. Basic formatting of *italics* and **bold** is supported. This *can be **nested** like* so.

:mi md-18|home:
:mi-outlined red|check_circle:
:mi-round md-36 blue|check_circle:
:mi-two-tone md-48 green-svg|check_circle:
:mi-sharp green-svg md-48|chrome_reader_mode:

## kroki

```plantuml
@startuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: another authentication Response
@enduml
```


> [!ATTENTION]
> An alert of type 'attention' using global style 'callout'.

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>  
<note>  
  <to>Tove</to>  
  <from>Jani</from>  
  <heading>Reminder</heading>  
  <body>Don't forget me this weekend!</body>  
</note>  
```

```markdown
<!-- Hover to play, unhover to pause (default) -->
![](https://raw.githubusercontent.com/gbodigital/docsify-gifcontrol/master/docs/charlie.gif)
```

![](https://raw.githubusercontent.com/gbodigital/docsify-gifcontrol/master/docs/charlie.gif)

## Lists

### Ordered list

1. Item 1
2. A second item
3. Number 3
4. Ⅳ

*Note: the fourth item uses the Unicode character for [Roman numeral four][2].*

### Unordered list

* An item
* Another item
* Yet another item
* And there's more...

## Paragraph modifiers

### Code block

    Code blocks are very useful for developers and other people who look at code or other things that are written in plain text. As you can see, it uses a fixed-width font.

You can also make `inline code` to add code into other things.

### Quote

> Here is a quote. What this is should be self explanatory. Quotes are automatically indented when they are used.

## Headings

There are six levels of headings. They correspond with the six levels of HTML headings. You've probably noticed them already in the page. Each level down uses one more hash character.

### Headings *can* also contain **formatting**

### They can even contain `inline code`

Of course, demonstrating what headings look like messes up the structure of the page.

I don't recommend using more than three or four levels of headings here, because, when you're smallest heading isn't too small, and you're largest heading isn't too big, and you want each size up to look noticeably larger and more important, there there are only so many sizes that you can use.

## URLs

URLs can be made in a handful of ways:

* A named link to [MarkItDown][3]. The easiest way to do these is to select what you want to make a link and hit `Ctrl+L`.
* Another named link to [MarkItDown](http://www.markitdown.net/)
* Sometimes you just want a URL like <http://www.markitdown.net/>.

## Horizontal rule

A horizontal rule is a line that goes across the middle of the page.

---

It's sometimes handy for breaking things up.

## Images

Markdown can also contain images. I'll need to add something here sometime.

From Edward Tufte, Visual Display of Quantitative Information, page 92.
![Visual Display of Quantitative Information](https://edwardtufte.github.io/tufte-css/img/exports-imports.png)

F.J. Cole, “The History of Albrecht Dürer’s Rhinoceros in Zooological Literature,” Science, Medicine, and History: Essays on the Evolution of Scientific Thought and Medical Practice (London, 1953), ed. E. Ashworth Underwood, 337-356. From page 71 of Edward Tufte’s Visual Explanations.
![Rhinoceros ](https://edwardtufte.github.io/tufte-css/img/rhino.png "The Rhinoceros ")

Like links, Images also have a footnote style syntax.
If you need a full-width figure, give it the fullwidth class. Make sure that’s inside an article, and it will take up (almost) the full width of the screen. This approach is demonstrated below using Edward Tufte’s English translation of the Napoleon’s March data visualization. From Beautiful Evidence, page 122-124.

![Alt text][id]

With a reference later in the document defining the URL location

[id]: https://edwardtufte.github.io/tufte-css/img/napoleons-march.png  "Napoleon’s March "

## Finally

There's actually a lot more to Markdown than this. See the official [introduction][4] and [syntax][5] for more information. However, be aware that this is not using the official implementation, and this might work subtly differently in some of the little things.

  [1]: http://daringfireball.net/projects/markdown/
  [2]: http://www.fileformat.info/info/unicode/char/2163/index.htm
  [3]: http://www.markitdown.net/
  [4]: http://daringfireball.net/projects/markdown/basics
  [5]: http://daringfireball.net/projects/markdown/syntax

## GitHub Flavored Markdown

GitHub.com uses its own version of the Markdown syntax that provides an additional set of useful features, many of which make it easier to work with content on GitHub.com

### Strikethrough

Any word wrapped with two tildes (like ~~this~~) will appear crossed out

### Syntax highlighting

Here’s an example of how you can use syntax highlighting with GitHub Flavored Markdown:

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

```java
public static boolean isPalindrome(final String originalString) {
  String reversedString = new StringBuilder(originalString).reverse().toString(); return originalString.equals(reversedString);
}
```

```json
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

```bash
tail -f /opt/jalios/logs.txt
```

### Task Lists

Solar System Exploration, 1950s – 1960s :

* [ ] Mercury
* [x] Venus
* [x] Earth (Orbit/Moon)
* [x] Mars
* [ ] Jupiter
* [ ] Saturn
* [ ] Uranus
* [ ] Neptune
* [ ] Comet Haley

### Tables

You can create tables by assembling a list of words and dividing them with hyphens `-` (for the first row), and then separating each column with a pipe `|`:

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

Right aligned columns

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

## markdown-it

### [Custom containers](https://github.com/markdown-it/markdown-it-container)

Plugin for creating block-level custom containers for markdown-it markdown parser:

::: warning
*here be dragons*
:::

### Footnotes

Footnotes allow you to add notes and references without cluttering the body of the document. When you create a footnote, a superscript number with a link appears where you added the footnote reference. Readers can click the link to jump to the content of the footnote at the bottom of the page.

Here's a simple footnote,[^1] and here's a longer one.[^bignote]

[^1]: This is the first footnote.

[^bignote]: Here's one with multiple paragraphs and code.

    Indent paragraphs to include them in the footnote.
